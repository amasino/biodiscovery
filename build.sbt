import AssemblyKeys._

assemblySettings

name := "genomics"

version := "0.1"

scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
	"org.scalatest" %% "scalatest" % "2.0.M4" % "test",
	"com.github.scala-incubator.io" %% "scala-io-core" % "0.4.0",
	"com.github.scala-incubator.io" %% "scala-io-file" % "0.4.0"
)

scalacOptions += "-deprecation"

scalacOptions += "unchecked"

mainClass in assembly := Some("com.bd.probes.ui.Main")

//skip tests during assembly
test in assembly := {}

jarName in assembly := "bioDisocvery.jar"
