PROJECT NAME: genomics
CREATED ON: 2013-01-10
AUTHOR: amasino
COMPANY: amasino

This project is a demo project for BioDiscovery. It allows import of large data files arranged by Chromosome.
On import files are parsed to sub directories by chromosome and subparsed by position on chromosome. After a 
file is imported it values associated with Chromsome regions can be queried.

Building the application:
An executable jar is available in the Downloads section of the Bitbucket repository. The application can be started in
the standard manner from the command line

java -jar bioDiscovery.jar

The source code project uses sbt with the sbt-assembly plugin. From the project's root directory, executing the 
following command will build the executable jar (without tests) 

sbt assembly

Application usage:
When started, the application will ask for directory to use as the sample file storage directory for the session. It is
recommended to use the default user-home-dir/.biodis. Simply pressing enter at application startup will select this 
option. 

Once the storage directory has been specified, a command prompt is displayed. Enter help for an explanation of all
commands. 

A typical workflow might be as follows (# indicates added comments. These are not displayed in actual application):
In this example, the file probes.txt is imported and then queried against.

#import the file using the import command. The arguments to the command are the path to the file to be imported
and the name to be used for the imported sample.
Enter command>>import /path/to/probes.txt probes
#list the available samples
Enter command>>list   
probes
probes-short
#specify the sample to use for querying
Enter command>>use probes-short
#show the currently active sample
Enter command>>sample
probes_short
#execute queries
Enter command>>qry chr1:0-10000
No values in query range
Enter command>>qry chr1:0-25000
chr1	15253	15278	-0.05000932514667511
Enter command>>qry chr1:0-50000
chr1	15253	15278	-0.05000932514667511
chr1	48168	48193	-0.5084637403488159
#quit the application
Enter command>>q
