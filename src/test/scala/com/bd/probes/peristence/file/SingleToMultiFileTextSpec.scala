/**
 * masinoa
 * Jan 11, 2013
 * BioDiscovery
 */
package com.bd.probes.peristence.file
import org.scalatest.FunSpec
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.matchers.ShouldMatchers._
import org.scalatest.BeforeAndAfter
import com.bd.probes.persistence.file.SingleToMultiFileText

/**
 *
 */
class SingleToMultiFileTextSpec extends FunSpec with GivenWhenThen with ShouldMatchers with BeforeAndAfter {

  def fixture() = {
    new {
      val test_file = "/Users/masinoa/exit4b/dev/sandbox/biodiscovery/genomics/data/probes.txt"
      val storageDir = "/Users/masinoa/exit4b/dev/sandbox/biodiscovery/genomics/data/storage"
      val saveAs = "probes"
      val importer = SingleToMultiFileText(storageDir, 10000)
    }
  }
  
  val f = fixture()
  
  describe("A SingleToMultiFileText"){
    it("should import a large file into multiple smaller files with a fixed dir structure"){
    	val status = f.importer.importFile(f.test_file, f.saveAs, "\t", 1)
    	status.msg should equal(None)
    	status.success should equal(true)
    }
  }
  
}