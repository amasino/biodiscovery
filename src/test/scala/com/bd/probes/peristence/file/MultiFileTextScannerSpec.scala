/**
 * masinoa
 * Jan 11, 2013
 * BioDiscovery
 */
package com.bd.probes.peristence.file
import org.scalatest.FunSpec
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.matchers.ShouldMatchers._
import org.scalatest.BeforeAndAfter
import com.bd.probes.persistence.file.SingleToMultiFileText
import com.bd.probes.persistence.file.MultiFileTextScanner
import com.bd.probes.ChromosomeRange
import scala.compat.Platform
import com.bd.probes.Sample

/**
 *
 */
class MultiFileTextScannerSpec extends FunSpec with GivenWhenThen with ShouldMatchers with BeforeAndAfter {

  def fixture() = {
    
    new {
      val storage_path = "/Users/masinoa/exit4b/dev/sandbox/biodiscovery/genomics/data/storage"
      val sample_path = "probes"
      val sample = new Sample(sample_path)
      val scanner = MultiFileTextScanner(storage_path)
    }
  }

  val f = fixture()

  describe("A MultiFileTextScanner") {
    it("should enable queries for Chromosome data") {
      given("a query contained on one ChromosomeRange object")
      f.scanner.query(f.sample, ChromosomeRange("1", 0, 11203235)) match {
        case Some(l) => l.length should equal(10003)
        case _ => fail()
      }
      
      f.scanner.query(f.sample, ChromosomeRange("1", 21441522, 21461482)) match {
        case Some(l) => l.length should equal(15)
        case _ => fail()
      }
      
      f.scanner.query(f.sample, ChromosomeRange("X", 152025937, 152025961)) match {
        case Some(l) => l.length should equal(3)
        case _ => fail()
      }
      
      given("a query contained on one Chromosome using a String")
      f.scanner.query(f.sample, "chr1:0-11203235") match {
        case Some(l) => l.length should equal(10003)
        case _ => fail()
      }
      
      f.scanner.query(f.sample, "chr1:21441522-21461482") match {
        case Some(l) => l.length should equal(15)
        case _ => fail()
      }
      
      f.scanner.query(f.sample, "chrX:152025937-152025961") match {
        case Some(l) => l.length should equal(3)
        case _ => fail()
      }
      
      f.scanner.query(f.sample, "chrY:2650141-2650450") match {
        case Some(l) => l.length should equal(3)
        case _ => fail()
      }
      
      given("a query spanning exactly two ChromosomeRange objects")
      val cl = ChromosomeRange("1", 249222470, Int.MaxValue) //6
      val cr = ChromosomeRange("2", 0, 29443) //29
      val tic = Platform.currentTime
      val q1 = f.scanner.query(f.sample, cl, cr) 
      val toc = Platform.currentTime
      println("Query ran in: %d ms".format(toc-tic))
      q1 match {
        case Some(l) => l.length should equal(35)
        case _ => fail
      }
      
      given("a query spanning exactly two Chromosomes using Strings")
      f.scanner.query(f.sample, "chr1:249222470-chr2:29443") match {
        case Some(l) => l.length should equal(35)
        case _ => fail
      }
      
      f.scanner.query(f.sample, "chr22:51244044-chrX:168489") match {
        case Some(l) => l.length should equal(2)
        case _ => fail
      }
      
      given("a query spanning three or more ChromosomeRange objects")
      val cl2 = ChromosomeRange("1", 249228390, 249228415)
      val cr2 = ChromosomeRange("3", 61892, 61917)
      val tic2 = Platform.currentTime
      val q2 = f.scanner.query(f.sample, cl2, cr2)
      val toc2 = Platform.currentTime
      println("Query ran in: %d ms".format(toc2-tic2))
      q2 match {
        case Some(l) => l.length should equal(214140)
        case _ => fail
      }
      
      given("a query spanning three or more Chromosomes using Strings")
      f.scanner.query(f.sample, "chr1:249228390-chr3:61917") match {
        case Some(l) => l.length should equal(214140)
        case _ => fail
      }

    }
  }

}