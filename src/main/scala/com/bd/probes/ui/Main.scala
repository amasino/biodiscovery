/**
 * masinoa
 * Jan 12, 2013
 * BioDiscovery
 */
package com.bd.probes.ui
import com.bd.probes.persistence.file.FileSampleManager
import com.bd.probes.persistence.file.MultiFileTextScanner
import com.bd.probes.persistence.file.SingleToMultiFileText
import com.bd.probes.persistence.file.DataFileImporter

/**
 *
 */
object Main {

  def main(args: Array[String]): Unit = {
    CommandLineUI.showStartMsg
    CommandLineUI.readStorageDir match {
      case "q" => CommandLineUI.sayGoodBye()
      case s => {
        val dfi  = SingleToMultiFileText(s)
        val sm  = FileSampleManager(s)
        CommandLineUI.run(dfi, sm, MultiFileTextScanner(s))
      }
    }
    
  }

}