/**
 * masinoa
 * Jan 12, 2013
 * BioDiscovery
 */
package com.bd.probes.ui
import java.io.File
import com.bd.probes.persistence.file.DataFileImporter
import com.bd.probes.persistence.file.FileSampleManager
import com.bd.probes.query.ChromosomeQuery
import com.bd.probes.persistence.file.ImportStatus
import com.bd.probes.Sample
import com.bd.probes.persistence.SampleManager
import com.bd.probes.query.ChromosomeQueryException

/**
 * A simple command line interface for querying probe files
 */
object CommandLineUI {

  def showStartMsg(): Unit = {
    println(startmsg)
  }
  
  def sayGoodBye(): Unit = {
    println(goodbye)
  }

  def readStorageDir(): String = {
    println(storagemsg)
    val sd = readLine
    if (sd.toLowerCase == "q") "q"
    else {
      val f = if (sd.trim.length == 0) "%s%s.biodis".format(System.getProperty("user.home"), File.separator) else sd
      val file = new File(f)
      if(file.exists && file.isDirectory){
        f
      }else if(file.exists){
        println("Error, file exists: %s".format(f))
        readStorageDir
      }else{
        println("Creating directory: %s".format(f))
        file.mkdir
        f
      }
    }
  }
  
  def showHelp() : Unit = {
    val import_file = "import [/full/path/to/sample/file] [saveAs]\n\tImport a sample file into BioDiscovery Storage Directory and store as saveAs"
    val list_sample = "list\n\tList samples in BioDiscovery Storage Directory"
    val use = "use [this_sample]\n\tSet current sample to this_sample" 
    val sample = "sample\n\tDisplays the current sample"
    val query ="qry [query_string]\n\twhere query_string must be of form\n\t\tchr$:#-# OR chr$:#-chr$:#\n\tand $ is in [1-22,X,Y] and # is an numeric position.\n\tNOTE: This command requires prior execution of use command"
    List(import_file, list_sample, use, sample, query) foreach{ cmd => println(cmd+"\n")}
  }
  
  def readCmdOrQuit() : Option[String] = {
    readLine.trim match{
        case "q" => None
        case "" => readCmdOrQuit
        case s => Some(s)
    }
  }
  
  def import_file(file:Option[String], saveAs:Option[String], dfi: DataFileImporter, sm: SampleManager) : Unit = {
    def attempt_import(f:String, sa: String) = {
      try{
        println("Importing file. This may take several seconds ...")
        dfi.importFile(f, sa, "\t", 1) match {
          case ImportStatus(true, _) => println("File imported successfully")
          case _ => println("Error importing file. Please ensure path is correctly specified.")
        }
      }catch{
        //TODO error handling
        case _ => println("Error importing file") 
      } 
    }
    
   file match{
     case Some(f) => saveAs match{
       case Some(sa) => {
        if(sm.listSamples.contains(sa)){
          println("Sample exists. Enter y to overwrite, n to enter new name, q to quit command.")
          readCmdOrQuit match{
            case Some("y") => {
              sm.deleteSample(new Sample(sa))
              attempt_import(f,sa)
            }
            case Some("n") => import_file(Some(f), None, dfi, sm)
            case _ => println("Exiting import file command")
          }
        }else attempt_import(f,sa) 
       }
       case _ => {
         println("Please enter name for sample, q to quit command.")
         readCmdOrQuit match{
           case Some(sa) => import_file(Some(f), Some(sa), dfi, sm)
           case _ => println("Exiting import file command")
         }
       }
     }
     case _ => {
       println("Please enter path of file to import, q to quit command.")
       readCmdOrQuit match{
         case Some(f) => import_file(Some(f), saveAs, dfi, sm)
         case _ => println("Exiting import file command")
       }
     }
   }
  }
  
  def use(sampleName: Option[String], sm:SampleManager) : Option[Sample] = {
    sampleName match{
      case Some(s) => {
        val sample = Sample(s)
        if(sm.sampleExists_?(sample))Some(sample)
        else{
          println("This sample does not exist")
          use(None, sm)
        }
      }
      case _ => {
        println("Please enter the sample name to use, q to quit command.")
        readCmdOrQuit match{
          case Some(sn) => use(Some(sn), sm)
          case _ => {
            println("Exiting use command")
            None
          }
        }
      }
    }
  }
  
  def query(qry: Option[String], cq: ChromosomeQuery[String], sample: Sample) : Unit = {
    qry match{
      case Some(q) => {
        try{
          cq.query(sample, q) match{
            case Some(l) => if(l.isEmpty) println("No values in query range")
            				else l foreach {crv => println("chr%s\t%d\t%d\t%s".format(crv.cr.id, crv.cr.start, crv.cr.end, crv.value))}
            case _ => println("No results for query")
          }
        }catch{
          case qe: ChromosomeQueryException => println("Invalid query format")
          case _ => ("Error occurred on query")
        }
        
      }
      case None => {
        println("Please enter query in form chr$:#-# OR chr$:#-chr$:#")
        readCmdOrQuit match{
          case Some(q) => query(Some(q), cq, sample)
          case _ => println("Exiting query command")
        }
      }
    }
  }
  

  def run(dfi: DataFileImporter, sm: FileSampleManager, cq: ChromosomeQuery[String], curSample: Option[Sample] = None): Unit = {
		  
    print("Enter command>>")
    val cmd = readLine.trim.toLowerCase
    val cmd_split = cmd.split(" ")
    val pref = cmd_split(0)
    pref match{
      case "q" => println(goodbye)
      case "help" => {
        showHelp
        run(dfi, sm, cq, curSample)
      }
      case "list" => {
        sm.listSamples() foreach {sample => println(sample.name)}
        run(dfi, sm, cq, curSample)
      }
      case "sample" =>{
        curSample match {
          case Some(s) => println(s.name)
          case _ => println("No sample in use. Try use command")
        }
        run(dfi, sm, cq, curSample)
      }
      case "import" => {
        cmd_split.length match{
          case 1 => import_file(None, None, dfi, sm)
          case 2 => import_file(Some(cmd_split(1)), None, dfi,sm)
          case _ => import_file(Some(cmd_split(1)), Some(cmd_split(2)), dfi, sm)
        }
        run(dfi, sm, cq, curSample)
      }
      case "use" =>{
        (cmd_split.length match{
          case 1 => use(None, sm)
          case _ => use(Some(cmd_split(1)), sm)
        }) match {
          case Some(sample) => run(dfi, sm, cq,  Some(sample))
          case _ => run(dfi, sm, cq, curSample)
        }
      }
      case "qry" =>{
        curSample match{
          case Some(cs) =>{
            cmd_split.length match{
              case 1 => query(None, cq, cs)
              case 2 => query(Some(cmd_split(1)), cq, cs)
            }
            run(dfi, sm, cq, Some(cs))
          }
          case _ => {
            println("qry command cannot be executed until use command is executed. Type help for more information")
            run(dfi, sm, cq, curSample)
          }
        }
      }
      case _ => {
        println("Invalid command")
        run(dfi, sm, cq, curSample)
      }
    }

  }

  lazy private val startmsg = "%s\nWelcome to the Simple Probe Query Tool\nBy BioDiscovery\n%s\n%s".format("%" * 50,
                               quitmsg, "%" * 50)

  lazy private val quitmsg = "Enter q or Q at any time to quit the application."
    
  lazy private val goodbye = "Thank you for using the Simple Query Tool. Good bye."

  lazy private val dflt_dir = "%s%s.biodis".format(System.getProperty("user.home"), File.separator)  
    
  lazy private val storagemsg = "%s\n%s%s".format("Please enter the path to your BioDiscovery Storage Directory",
    "Press Enter for ", dflt_dir)
}