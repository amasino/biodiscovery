/**
 * masinoa
 * Jan 12, 2013
 * BioDiscovery
 */
package com.bd.probes

/**
 *
 */
case class Sample(val name: String) extends Ordered[Sample]{
  def compare(that: Sample) = this.name compareTo(that.name)
}