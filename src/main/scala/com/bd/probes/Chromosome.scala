package com.bd.probes

/**
 * @parameter id - the chromosome number [1-22, X, Y]
 * @parameter start - starting position
 * @parameter end - ending position
 */
case class ChromosomeRange(val id: String, val start: Int, val end: Int) extends Ordered[ChromosomeRange] {
	require(start>=0)
	require(end>=start)
	require(ChromosomeRange.validId_?(id))

	def idToInt = if(id=="X") ChromosomeRange.X else if(id=="Y") ChromosomeRange.Y else id.toInt
	
	def compare(that: ChromosomeRange) = {
	  if(that.idToInt==idToInt){
	    this.start - that.start
	  }else this.idToInt - that.idToInt
	}
	
	//TODO implement equals and hashCode methods that align with compare method
}

object ChromosomeRange{
  lazy val VALID_IDS = List("1","2","3","4","5","6","7","8","9","10",
	             "11","12","13","14","15","16","17","18","19",
	             "20","21","22","X","Y")

  lazy val X = VALID_IDS.indexOf("X")
  
  lazy val Y = VALID_IDS.indexOf("Y")
  
  def validId_?(id:String) = VALID_IDS.contains(id)
  
  
}

case class ChromosomeRangeValue[T](val cr: ChromosomeRange, value: T)