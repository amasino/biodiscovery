/**
 * masinoa
 * Jan 12, 2013
 * BioDiscovery
 */
package com.bd.probes.persistence.file
import java.io.File
import com.bd.probes.persistence.SampleManager
import com.bd.probes.Sample

case class FileSampleManager(val storageDir : String) extends SampleManager{

  lazy private val dir = new File(storageDir)
  lazy private val ignoreList = List(".DS_Store") //MAC OS hack
  
  def listSamples() : List[Sample] = {
    dir.listFiles match{
      case null => List[Sample]()
      case a => a filterNot {file => ignoreList.contains(file.getName) }map {file => Sample(file.getName)} toList
    }
  }
  
  def deleteSample(sample:Sample) : Boolean = {
    val f = new File("%s%s%s".format(storageDir, File.separator, sample.name))
    if(f.exists)f.delete() else false
  }
  
}


