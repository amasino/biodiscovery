/**
 * masinoa
 * Jan 11, 2013
 * BioDiscovery
 */
package com.bd.probes.persistence.file
import scalax.io._
import scalax.io.Resource
import scalax.io.Codec
import scalax.io.Line

/**
 *
 */
trait DataFileImporter {
	
	/*codec for imported file*/
	implicit val codec = Codec.UTF8
  
	/**
	 * @parameter sourcePath - full path of file to be imported
	 * @parameter del - column delimiter, default is tab
	 * source file should be a delimited ASCII text file with first four columns:
	 * Chromosome, Start, End, Value
	 * with formats
	 * chrZ (where Z is #, ##, X, or Y), Int, Int, String
	 * ASSUMPTION - source file rows are sorted by chromosome number and start / end position in ascending order
	 */
	def openLineReader(sourcePath: String, del: String = "\t", dropCount: Int = 0) : Option[LongTraversable[String]] = {
	  try{
	    val lines = Resource.fromFile(sourcePath).lines(terminator=Line.Terminators.NewLine)
	    if(dropCount==0)Some(lines) else Some(lines.drop(dropCount))
	  }catch{
	    //TODO error handling
	    case _ => None
	  }
	}
	
	def importFile(sourcePath: String, saveAs: String, del: String = "\t", skipLines: Int = 0) : ImportStatus
	
}

case class ImportStatus(success:Boolean, msg: Option[String] = None)