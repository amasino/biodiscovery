/**
 * masinoa
 * Jan 11, 2013
 * BioDiscovery
 */
package com.bd.probes.persistence.file
import scalax.file.Path
import java.io.File
import scalax.file.NotFileException
import scalax.io.ScalaIOException
import scala.collection.mutable.ListBuffer
import scalax.io.Output
import scalax.io.Resource

/**
 * @parameter storageDir - root directory where all data files are stored when imported
 * @parameter N - number of lines per subfile, default 10000
 * @createSD - boolean indicates if storageDir should be created if it doesn't exist
 * imports 4 column imported files to series of smaller files organized by directory and base.
 * Input file - tab delimited format with first 4 columns:
 * Chromosome, Start, End, Value
 * Output - creates a directory structure by chromosome with data for each chromosome divided
 * into M/N files where M is the number of data points for the chromosome
 */
case class SingleToMultiFileText(val storageDir: String, val N: Int = 10000, val createSD: Boolean = true) extends DataFileImporter {

  lazy private val root: File = {
    val f = new File(storageDir)
    if (createSD && !f.exists) f.mkdirs
    if (!f.isDirectory) throw new IllegalArgumentException
    f
  }

  private def createPath(parent_dir: String, chromosome: String, start: String, end: String): File = {
    val fs = File.separator
    val dir = new File("%s%s%s%s%s".format(root.getPath, fs, parent_dir, fs, chromosome))
    if (!dir.exists) dir.mkdirs()
    val f = new File("%s%s%s%s%s".format(dir.getPath, fs, start, SingleToMultiFileText.position_sep, end))
    f
  }

  def importFile(sourcePath: String, saveAs: String, del: String = "\t", skipLines: Int = 0): ImportStatus = {
    val f = new File(sourcePath)
    if (f.exists && f.isFile) {
      openLineReader(sourcePath, del, skipLines) match {
        case Some(lines) => {
          try {
            val last_pack: Package = (Package() /: lines) { (pack, line) =>
              val (line_cid, line_start, line_end, v) = line.split(del) match {
                case s: Array[String] => if (s.length > 3) (s(0).substring(3).toUpperCase, s(1).trim(), s(2).trim(), s(3).trim()) else ("", "", "", "")
                //TODO error handling
                case _ => ("", "", "", "")
              }
              val nl = "%s%s%s%s%s\n".format(line_start, SingleToMultiFileText.delimiter, line_end, SingleToMultiFileText.delimiter, v)
              val cur_cid = pack.crid match {
                case Some(id) => id
                case _ => line_cid
              }
              val cur_start = pack.start match {
                case Some(s) => s
                case _ => line_start
              }

              //if on same chromosome and accumulated lines less than max lines per file keep accumulating
              if (cur_cid == line_cid && pack.accumulatedLines.length < N) {
                //add line to accumulated lines and package
                //files appear to be sorted on start position but end  position may be interleaved so must check end
                val cur_end = pack.end match {
                  case Some(e) => if (line_end.toInt > e.toInt) line_end else e
                  case _ => line_end
                }
                Package(Some(cur_cid), Some(cur_start), Some(cur_end), pack.accumulatedLines += nl)
              } else {
                //write lines to file and start new accumulatedLines
                val cur_end = pack.end match {
                  case Some(e) => e
                  case _ => cur_start
                }
                pack.writeToFile(createPath(saveAs, cur_cid, cur_start, cur_end))
                Package(Some(line_cid), Some(line_start), Some(line_end), ListBuffer[String](nl))
              }
            }
            if (!last_pack.accumulatedLines.isEmpty) last_pack.writeToFile(createPath(saveAs, last_pack.crid.get, last_pack.start.get, last_pack.end.get))
            ImportStatus(true)
          } catch {
            case iob: IndexOutOfBoundsException => ImportStatus(false, Some("Index out of bounds exception on file import. Likely indicates missing column data"))
            case sio: ScalaIOException => ImportStatus(false, Some("IO exception. Does the sourcePath exist?"))
            case iae: IllegalArgumentException => ImportStatus(false, Some("Illegal Arg Exception. Likely cause is error in start, end chromosome positions or Chromosome ID"))
            case _ => ImportStatus(false, Some("Unknown Error"))
          }
        }
        case _ => ImportStatus(false, Some("Error opening source file: %s".format(sourcePath)))
      } //end match
    } //end if
    else ImportStatus(false, Some("File does not exist or is a directory: %s".format(sourcePath)))
  }

}

object SingleToMultiFileText {
  val position_sep = "_"
  val delimiter = "\t"
}

private case class Package(crid: Option[String] = None, start: Option[String] = None, end: Option[String] = None, accumulatedLines: ListBuffer[String] = new ListBuffer[String]) {

  def writeToFile(f: File) = {
    val out: Output = Resource.fromFile(f.getPath)
    out.writeStrings(accumulatedLines)
  }
}