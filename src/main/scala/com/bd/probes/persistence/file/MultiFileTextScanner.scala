/**
 * masinoa
 * Jan 10, 2013
 * BioDiscovery
 */
package com.bd.probes.persistence.file
import com.bd.probes.ChromosomeRange
import java.io.File
import scalax.io.LongTraversable
import scalax.io.Resource
import scalax.io.Line
import com.bd.probes.ChromosomeRangeValue
import com.bd.probes.query.ChromosomeQuery
import com.bd.probes.Sample

/**
 *
 */
case class MultiFileTextScanner(val storageDir : String) extends ChromosomeQuery[String] {

  private def extractStartEnd(fileName: String) = {
    fileName.split(SingleToMultiFileText.position_sep) match {
      case a: Array[String] => (a(0).toInt, a(1).toInt)
      case _ => (-1, -1)
    }
  }

  private def findFiles(sampleDir: String, c: ChromosomeRange): Array[File] = {
    val dir = new File("%s%s%s%s%s".format(storageDir, File.separator, sampleDir, File.separator, c.id))
    if (!dir.isDirectory()) return Array[File]()
    dir.listFiles() filter { f =>
      val (start, end) = extractStartEnd(f.getName)
      (start <= c.start && end >= c.start) || (start <= c.end && start >= c.start)
    }
  }

  private def readLines(f: File): Option[LongTraversable[String]] = {
    try {
      Some(Resource.fromFile(f.getPath()).lines(terminator = Line.Terminators.NewLine))
    } catch {
      //TODO error handling
      case _ => None
    }
  }

  private def parseLine(line: String): Option[(Int, Int, String)] = {
    line.split(SingleToMultiFileText.delimiter) match {
      case s: Array[String] => if (s.length > 2) Some((s(0).trim.toInt, s(1).trim.toInt, s(2).trim)) else None
      case _ => None
    }
  }

  private def buildCRV(cid: String, line: String): Option[ChromosomeRangeValue[String]] = {
    parseLine(line) match {
      case Some((start, end, value)) => Some(ChromosomeRangeValue(ChromosomeRange(cid, start, end), value))
      case _ => None
    }
  }

  private def inRange(container: ChromosomeRange, contained: ChromosomeRange) = {
    val start = contained.start
    val end = contained.end
    val cstart = container.start
    val cend = container.end
    (start <= cstart && end >= cstart) || (start <= cend && start >= cstart)
  }

  def query(s: Sample, c: ChromosomeRange): Option[List[ChromosomeRangeValue[String]]] = {
    findFiles(s.name, c) match {
      case a: Array[File] => {
        Some((a map { f =>
          val (start, end) = extractStartEnd(f.getName)
          val loadEntireFile = start >= c.start && end <= c.end
          readLines(f) match {
            case Some(lines) =>
              {
                val m = if (loadEntireFile) {
                  lines map { line => buildCRV(c.id, line) } flatten
                } else {
                  (lines map { line =>
                    buildCRV(c.id, line) match {
                      case Some(crv) => {
                        if (inRange(c, crv.cr)) Some(crv) else None
                      }
                      case _ => None
                    }
                  }).flatten
                }
                m
              }
            case _ => LongTraversable[(ChromosomeRangeValue[String])]()
          }
        }).toList.flatten)
      }
      case _ => None
    }
  }

  private def forceQueryToList(s: Sample, cr: ChromosomeRange): List[ChromosomeRangeValue[String]] = {
    query(s, cr) match {
      case Some(l) => l
      case _ => List[ChromosomeRangeValue[String]]()
    }
  }

  def query(s: Sample, start: ChromosomeRange, stop: ChromosomeRange): Option[List[ChromosomeRangeValue[String]]] = {
    val delta = stop.compare(start)
    val (left, right) = if (delta > 0) (start, stop) else (stop, start)
    if (left.id == right.id) {
      //only query on one chromosome
      query(s, ChromosomeRange(left.id, left.start, List(left.end, right.end).max))
    } else if (right.idToInt - left.idToInt == 1) {
      //only query on the specified chromosomes
      Some(forceQueryToList(s, left) ::: forceQueryToList(s, right))
    } else {
      val rng = (left.idToInt + 1) until right.idToInt
      Some(forceQueryToList(s, left) ::: (rng.toList map {i => 
      	val id = if(i==ChromosomeRange.X) "X" else if(i==ChromosomeRange.Y) "Y" else i.toString
      	forceQueryToList(s, ChromosomeRange(id, 0, Int.MaxValue))
      }).flatten ::: forceQueryToList(s, right))
    }
  }

}

