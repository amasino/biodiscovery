package com.bd.probes.persistence
import com.bd.probes.Sample

trait SampleManager {
	
  def listSamples() : List[Sample]
  
  def deleteSample(sample: Sample) : Boolean
  
  def sampleExists_?(sample: Sample) : Boolean = {
    listSamples.contains(sample)
  }
}