/**
 * masinoa
 * Jan 10, 2013
 * BioDiscovery
 */
package com.bd.probes.query
import com.bd.probes.ChromosomeRange
import com.bd.probes.ChromosomeRangeValue
import scala.util.matching.Regex
import com.bd.probes.Sample

/**
 * base trait for querying data stores that associate chromosome ranges with data values
 * e.g. probe file associates log ratio of sample DNA amount to reference amount with chromosome, start, end
 */
trait ChromosomeQuery[T]{
	
  /**
   * @parameter c - ChromosomeRange to query for value
   * @return Option[T] - value associated with c
   */
  def query(s: Sample, c: ChromosomeRange) : Option[List[ChromosomeRangeValue[T]]]
  
  /**
   * @parameter start - starting ChromosomeRange to query for values
   * @parameter stop - stopping ChromosomeRange to query for values
   * @return Map[ChromosomeRange, Option[T]] - values associated with each ChromosomeRange, None if no value for range
   */
  def query(s: Sample, start: ChromosomeRange, stop: ChromosomeRange) : Option[List[ChromosomeRangeValue[T]]]
  
  def query(s: Sample, q:String) : Option[List[ChromosomeRangeValue[T]]] = {
    //chr18:0-60000000
    //chr3:5000-chr5:8000
    val SQREG = """^CHR([\d\w]+)?:(\d+)?-(\d+)?$""".r
    val TQREG = """^CHR([\d\w]+)?:(\d+)?-CHR([\d\w]+)?:(\d+)?$""".r
    
    val tq = q.trim.toUpperCase()
    SQREG.findFirstIn(tq) match {
      case Some(m) => {
        val SQREG(crid, lb, rb) = tq
        if(lb.toInt<rb.toInt && ChromosomeRange.validId_?(crid))
          query(s, ChromosomeRange(crid,lb.toInt,rb.toInt))
        else 
          throw new ChromosomeQueryException(q)
      }
      case _ => {
        TQREG.findFirstIn(tq) match {
          case Some(m) => {
            val TQREG(crid1, lb1, crid2, rb2) = tq
            if(ChromosomeRange.validId_?(crid1) && ChromosomeRange.validId_?(crid2))
              query(s, ChromosomeRange(crid1,lb1.toInt,Int.MaxValue), ChromosomeRange(crid2, 0,  rb2.toInt))
            else
              throw new ChromosomeQueryException(q)
          }
          case _ => throw new ChromosomeQueryException(q)
        }
      }
    }
  }
  
}

class ChromosomeQueryException(val q: String) extends Exception{
  def dflt_msg = "Invalid Query Format: %s".format(q)
}